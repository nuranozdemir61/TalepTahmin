﻿namespace Proje.Forms
{
    partial class FormYoneticiPaneli
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormYoneticiPaneli));
            this.btnStok = new System.Windows.Forms.Button();
            this.btnTalepTahmini = new System.Windows.Forms.Button();
            this.brtnSiparis = new System.Windows.Forms.Button();
            this.btnUrunOnay = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnStok
            // 
            this.btnStok.Location = new System.Drawing.Point(23, 33);
            this.btnStok.Name = "btnStok";
            this.btnStok.Size = new System.Drawing.Size(100, 30);
            this.btnStok.TabIndex = 0;
            this.btnStok.Text = "Stok Bilgisi";
            this.btnStok.UseVisualStyleBackColor = true;
            this.btnStok.Click += new System.EventHandler(this.btnStok_Click);
            // 
            // btnTalepTahmini
            // 
            this.btnTalepTahmini.Location = new System.Drawing.Point(23, 81);
            this.btnTalepTahmini.Name = "btnTalepTahmini";
            this.btnTalepTahmini.Size = new System.Drawing.Size(100, 30);
            this.btnTalepTahmini.TabIndex = 1;
            this.btnTalepTahmini.Text = "Talep Tahmini";
            this.btnTalepTahmini.UseVisualStyleBackColor = true;
            this.btnTalepTahmini.Click += new System.EventHandler(this.btnTalepTahmini_Click);
            // 
            // brtnSiparis
            // 
            this.brtnSiparis.Location = new System.Drawing.Point(23, 129);
            this.brtnSiparis.Name = "brtnSiparis";
            this.brtnSiparis.Size = new System.Drawing.Size(100, 31);
            this.brtnSiparis.TabIndex = 2;
            this.brtnSiparis.Text = "Sipariş Verme";
            this.brtnSiparis.UseVisualStyleBackColor = true;
            this.brtnSiparis.Click += new System.EventHandler(this.brtnSiparis_Click);
            // 
            // btnUrunOnay
            // 
            this.btnUrunOnay.Location = new System.Drawing.Point(23, 178);
            this.btnUrunOnay.Name = "btnUrunOnay";
            this.btnUrunOnay.Size = new System.Drawing.Size(100, 34);
            this.btnUrunOnay.TabIndex = 3;
            this.btnUrunOnay.Text = "Ürün Onay";
            this.btnUrunOnay.UseVisualStyleBackColor = true;
            this.btnUrunOnay.Click += new System.EventHandler(this.btnUrunOnay_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(176, 68);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(182, 119);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // FormYoneticiPaneli
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(383, 260);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btnUrunOnay);
            this.Controls.Add(this.brtnSiparis);
            this.Controls.Add(this.btnTalepTahmini);
            this.Controls.Add(this.btnStok);
            this.Name = "FormYoneticiPaneli";
            this.Text = "Yönetici Paneli";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnStok;
        private System.Windows.Forms.Button btnTalepTahmini;
        private System.Windows.Forms.Button brtnSiparis;
        private System.Windows.Forms.Button btnUrunOnay;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}