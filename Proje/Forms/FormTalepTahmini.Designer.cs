﻿namespace Proje.Forms
{
    partial class FormTalepTahmini
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblUrunAdi = new System.Windows.Forms.Label();
            this.lblBaslangicTarihi = new System.Windows.Forms.Label();
            this.lblBitisTarihi = new System.Windows.Forms.Label();
            this.cmbUrunler = new System.Windows.Forms.ComboBox();
            this.cmbBaslangicTarihi = new System.Windows.Forms.ComboBox();
            this.cmbBitisTarihi = new System.Windows.Forms.ComboBox();
            this.btnTalepTahmini = new System.Windows.Forms.Button();
            this.btnTahminDetayı = new System.Windows.Forms.Button();
            this.btnCikis = new System.Windows.Forms.Button();
            this.talepTahmini = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lblUrunAdi
            // 
            this.lblUrunAdi.AutoSize = true;
            this.lblUrunAdi.Location = new System.Drawing.Point(29, 42);
            this.lblUrunAdi.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblUrunAdi.Name = "lblUrunAdi";
            this.lblUrunAdi.Size = new System.Drawing.Size(63, 17);
            this.lblUrunAdi.TabIndex = 0;
            this.lblUrunAdi.Text = "Ürün Adı";
            // 
            // lblBaslangicTarihi
            // 
            this.lblBaslangicTarihi.AutoSize = true;
            this.lblBaslangicTarihi.Location = new System.Drawing.Point(29, 84);
            this.lblBaslangicTarihi.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblBaslangicTarihi.Name = "lblBaslangicTarihi";
            this.lblBaslangicTarihi.Size = new System.Drawing.Size(109, 17);
            this.lblBaslangicTarihi.TabIndex = 1;
            this.lblBaslangicTarihi.Text = "Başlangıç Tarihi";
            // 
            // lblBitisTarihi
            // 
            this.lblBitisTarihi.AutoSize = true;
            this.lblBitisTarihi.Location = new System.Drawing.Point(29, 122);
            this.lblBitisTarihi.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblBitisTarihi.Name = "lblBitisTarihi";
            this.lblBitisTarihi.Size = new System.Drawing.Size(74, 17);
            this.lblBitisTarihi.TabIndex = 2;
            this.lblBitisTarihi.Text = "Bitiş Tarihi";
            // 
            // cmbUrunler
            // 
            this.cmbUrunler.FormattingEnabled = true;
            this.cmbUrunler.Location = new System.Drawing.Point(175, 42);
            this.cmbUrunler.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cmbUrunler.Name = "cmbUrunler";
            this.cmbUrunler.Size = new System.Drawing.Size(193, 24);
            this.cmbUrunler.TabIndex = 3;
            // 
            // cmbBaslangicTarihi
            // 
            this.cmbBaslangicTarihi.FormattingEnabled = true;
            this.cmbBaslangicTarihi.Location = new System.Drawing.Point(175, 84);
            this.cmbBaslangicTarihi.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cmbBaslangicTarihi.Name = "cmbBaslangicTarihi";
            this.cmbBaslangicTarihi.Size = new System.Drawing.Size(193, 24);
            this.cmbBaslangicTarihi.TabIndex = 4;
            this.cmbBaslangicTarihi.SelectedIndexChanged += new System.EventHandler(this.cmbBaslangicTarihi_SelectedIndexChanged);
            // 
            // cmbBitisTarihi
            // 
            this.cmbBitisTarihi.FormattingEnabled = true;
            this.cmbBitisTarihi.Location = new System.Drawing.Point(175, 122);
            this.cmbBitisTarihi.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cmbBitisTarihi.Name = "cmbBitisTarihi";
            this.cmbBitisTarihi.Size = new System.Drawing.Size(193, 24);
            this.cmbBitisTarihi.TabIndex = 5;
            // 
            // btnTalepTahmini
            // 
            this.btnTalepTahmini.Location = new System.Drawing.Point(33, 193);
            this.btnTalepTahmini.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnTalepTahmini.Name = "btnTalepTahmini";
            this.btnTalepTahmini.Size = new System.Drawing.Size(140, 28);
            this.btnTalepTahmini.TabIndex = 6;
            this.btnTalepTahmini.Text = "Talep Tahmini";
            this.btnTalepTahmini.UseVisualStyleBackColor = true;
            this.btnTalepTahmini.Click += new System.EventHandler(this.btnTalepTahmini_Click);
            // 
            // btnTahminDetayı
            // 
            this.btnTahminDetayı.Location = new System.Drawing.Point(33, 260);
            this.btnTahminDetayı.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnTahminDetayı.Name = "btnTahminDetayı";
            this.btnTahminDetayı.Size = new System.Drawing.Size(140, 28);
            this.btnTahminDetayı.TabIndex = 7;
            this.btnTahminDetayı.Text = "Tahmin Detayı";
            this.btnTahminDetayı.UseVisualStyleBackColor = true;
            this.btnTahminDetayı.Click += new System.EventHandler(this.btnTahminDetayı_Click);
            // 
            // btnCikis
            // 
            this.btnCikis.Location = new System.Drawing.Point(212, 260);
            this.btnCikis.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnCikis.Name = "btnCikis";
            this.btnCikis.Size = new System.Drawing.Size(100, 28);
            this.btnCikis.TabIndex = 8;
            this.btnCikis.Text = "Çıkış";
            this.btnCikis.UseVisualStyleBackColor = true;
            // 
            // talepTahmini
            // 
            this.talepTahmini.Location = new System.Drawing.Point(212, 196);
            this.talepTahmini.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.talepTahmini.Name = "talepTahmini";
            this.talepTahmini.Size = new System.Drawing.Size(156, 22);
            this.talepTahmini.TabIndex = 9;
            this.talepTahmini.TextChanged += new System.EventHandler(this.talepTahmini_TextChanged);
            // 
            // FormTalepTahmini
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(444, 321);
            this.Controls.Add(this.talepTahmini);
            this.Controls.Add(this.btnCikis);
            this.Controls.Add(this.btnTahminDetayı);
            this.Controls.Add(this.btnTalepTahmini);
            this.Controls.Add(this.cmbBitisTarihi);
            this.Controls.Add(this.cmbBaslangicTarihi);
            this.Controls.Add(this.cmbUrunler);
            this.Controls.Add(this.lblBitisTarihi);
            this.Controls.Add(this.lblBaslangicTarihi);
            this.Controls.Add(this.lblUrunAdi);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "FormTalepTahmini";
            this.Text = "Talep Tahmini";
            this.Load += new System.EventHandler(this.FormTalepTahmini_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblUrunAdi;
        private System.Windows.Forms.Label lblBaslangicTarihi;
        private System.Windows.Forms.Label lblBitisTarihi;
        private System.Windows.Forms.ComboBox cmbUrunler;
        private System.Windows.Forms.ComboBox cmbBaslangicTarihi;
        private System.Windows.Forms.ComboBox cmbBitisTarihi;
        private System.Windows.Forms.Button btnTalepTahmini;
        private System.Windows.Forms.Button btnTahminDetayı;
        private System.Windows.Forms.Button btnCikis;
        private System.Windows.Forms.TextBox talepTahmini;
    }
}