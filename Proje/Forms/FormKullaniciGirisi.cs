﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proje.Forms
{
    public partial class FormKullaniciGirisi : Form
    {
        public FormKullaniciGirisi()
        {
            InitializeComponent();
    
        }

        private void FormKullaniciGirisi_Load(object sender, EventArgs e)
        {
            cmbKullanici.Items.Add("Yönetici");
            cmbKullanici.Items.Add("Görevli");
            txtSifre.PasswordChar = '*';
        }
        
        private void btnGiris_Click(object sender, EventArgs e)
        {
            
            if (cmbKullanici.SelectedItem.ToString() == "Yönetici" && txtSifre.Text=="1234")
            {
                FormYoneticiPaneli yoneticiForm = new FormYoneticiPaneli();
                yoneticiForm.Show();
               
            }
            else if (cmbKullanici.SelectedItem.ToString() == "Görevli" && txtSifre.Text == "123")
            {
                FormGorevliPaneli gorevliForm = new FormGorevliPaneli();
                gorevliForm.Show();
            }
            else
            {
                MessageBox.Show("Kullanıcı adınız veya şifrenşz yanlış");
            }
        }
    }
}
