﻿using System;
using Excel = Microsoft.Office.Interop.Excel;
using System.Windows.Forms;
using System.IO;

namespace Proje.Forms
{
    public partial class FormSiparisVerme : Form
    {
        public FormSiparisVerme()
        {
            InitializeComponent();
        }

        private void btnCikis_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSiparisVer_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Siparişiniz verilmiştir.");
        }

        private void FormSiparisVerme_Load(object sender, EventArgs e)
        {
            cmbUrunler.Items.Add("Armut");
            cmbUrunler.Items.Add("Ayva");
            cmbUrunler.Items.Add("Biber");
            cmbUrunler.Items.Add("Çilek");
            cmbUrunler.Items.Add("Domates");
            cmbUrunler.Items.Add("Elma");
            cmbUrunler.Items.Add("Erik");
            cmbUrunler.Items.Add("Karpuz");
            cmbUrunler.Items.Add("Kavun");
            cmbUrunler.Items.Add("Patates");
            cmbUrunler.Items.Add("Salatalık");
            cmbUrunler.SelectedIndex = 0;
        }

        private void cmbUrunler_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(cmbUrunler.SelectedIndex == 5)
            {
                Excel.Application xlApp;
                Excel.Workbook xlWorkBook;
                Excel.Worksheet xlWorkSheet;
                Excel.Range range;

                xlApp = new Excel.Application();
                xlWorkBook = xlApp.Workbooks.Open(Directory.GetCurrentDirectory() + @"\data\data.xlsx", 0, true, 5, "", "", true, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
                xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(2);

                range = xlWorkSheet.UsedRange;

                var siparisMiktari = (double)(range.Cells[2, 7] as Excel.Range).Value2;

                txtSiparisMiktari.Text = siparisMiktari.ToString();
            }
        }
    }
}
