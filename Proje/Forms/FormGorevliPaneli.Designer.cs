﻿namespace Proje.Forms
{
    partial class FormGorevliPaneli
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblUrunAdi = new System.Windows.Forms.Label();
            this.lblTarih = new System.Windows.Forms.Label();
            this.lblFireMiktari = new System.Windows.Forms.Label();
            this.cmbUrunler = new System.Windows.Forms.ComboBox();
            this.cmbTarih = new System.Windows.Forms.ComboBox();
            this.txtFireMiktari = new System.Windows.Forms.TextBox();
            this.btnKaydet = new System.Windows.Forms.Button();
            this.btnCikis = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblUrunAdi
            // 
            this.lblUrunAdi.AutoSize = true;
            this.lblUrunAdi.Location = new System.Drawing.Point(56, 41);
            this.lblUrunAdi.Name = "lblUrunAdi";
            this.lblUrunAdi.Size = new System.Drawing.Size(48, 13);
            this.lblUrunAdi.TabIndex = 0;
            this.lblUrunAdi.Text = "Ürün Adı";
            // 
            // lblTarih
            // 
            this.lblTarih.AutoSize = true;
            this.lblTarih.Location = new System.Drawing.Point(73, 82);
            this.lblTarih.Name = "lblTarih";
            this.lblTarih.Size = new System.Drawing.Size(31, 13);
            this.lblTarih.TabIndex = 1;
            this.lblTarih.Text = "Tarih";
            // 
            // lblFireMiktari
            // 
            this.lblFireMiktari.AutoSize = true;
            this.lblFireMiktari.Location = new System.Drawing.Point(46, 123);
            this.lblFireMiktari.Name = "lblFireMiktari";
            this.lblFireMiktari.Size = new System.Drawing.Size(58, 13);
            this.lblFireMiktari.TabIndex = 2;
            this.lblFireMiktari.Text = "Fire Miktarı";
            // 
            // cmbUrunler
            // 
            this.cmbUrunler.FormattingEnabled = true;
            this.cmbUrunler.Location = new System.Drawing.Point(123, 38);
            this.cmbUrunler.Name = "cmbUrunler";
            this.cmbUrunler.Size = new System.Drawing.Size(150, 21);
            this.cmbUrunler.TabIndex = 3;
            // 
            // cmbTarih
            // 
            this.cmbTarih.FormattingEnabled = true;
            this.cmbTarih.Location = new System.Drawing.Point(123, 77);
            this.cmbTarih.Name = "cmbTarih";
            this.cmbTarih.Size = new System.Drawing.Size(150, 21);
            this.cmbTarih.TabIndex = 4;
            // 
            // txtFireMiktari
            // 
            this.txtFireMiktari.Location = new System.Drawing.Point(123, 121);
            this.txtFireMiktari.Name = "txtFireMiktari";
            this.txtFireMiktari.Size = new System.Drawing.Size(150, 20);
            this.txtFireMiktari.TabIndex = 5;
            this.txtFireMiktari.TextChanged += new System.EventHandler(this.txtFireMiktari_TextChanged);
            // 
            // btnKaydet
            // 
            this.btnKaydet.Location = new System.Drawing.Point(115, 162);
            this.btnKaydet.Name = "btnKaydet";
            this.btnKaydet.Size = new System.Drawing.Size(84, 33);
            this.btnKaydet.TabIndex = 6;
            this.btnKaydet.Text = "Kaydet";
            this.btnKaydet.UseVisualStyleBackColor = true;
            this.btnKaydet.Click += new System.EventHandler(this.btnKaydet_Click);
            // 
            // btnCikis
            // 
            this.btnCikis.Location = new System.Drawing.Point(205, 162);
            this.btnCikis.Name = "btnCikis";
            this.btnCikis.Size = new System.Drawing.Size(79, 33);
            this.btnCikis.TabIndex = 7;
            this.btnCikis.Text = "Çıkış";
            this.btnCikis.UseVisualStyleBackColor = true;
            this.btnCikis.Click += new System.EventHandler(this.btnCikis_Click);
            // 
            // FormGorevliPaneli
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(356, 259);
            this.Controls.Add(this.btnCikis);
            this.Controls.Add(this.btnKaydet);
            this.Controls.Add(this.txtFireMiktari);
            this.Controls.Add(this.cmbTarih);
            this.Controls.Add(this.cmbUrunler);
            this.Controls.Add(this.lblFireMiktari);
            this.Controls.Add(this.lblTarih);
            this.Controls.Add(this.lblUrunAdi);
            this.Name = "FormGorevliPaneli";
            this.Text = "Gorevli Paneli";
            this.Load += new System.EventHandler(this.FormGorevliPaneli_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblUrunAdi;
        private System.Windows.Forms.Label lblTarih;
        private System.Windows.Forms.Label lblFireMiktari;
        private System.Windows.Forms.ComboBox cmbUrunler;
        private System.Windows.Forms.ComboBox cmbTarih;
        private System.Windows.Forms.TextBox txtFireMiktari;
        private System.Windows.Forms.Button btnKaydet;
        private System.Windows.Forms.Button btnCikis;
    }
}