﻿using System;
using Excel = Microsoft.Office.Interop.Excel;
using System.Windows.Forms;

namespace Proje.Forms
{
    public partial class Grafik : Form
    {
        public Grafik()
        {
            InitializeComponent();
        }

        private void chart1_Click(object sender, EventArgs e)
        {
            fillChart();
        }

        private void fillChart()
        {
            Excel.Application xlApp;
            Excel.Workbook xlWorkBook;
            Excel.Worksheet xlWorkSheet;
            Excel.Range range;
            
            int rw = 0;
            int cl = 0;

            xlApp = new Excel.Application();
            xlWorkBook = xlApp.Workbooks.Open(@"C:\Users\user\Desktop\Proje-master (1)\Proje-master\data\data.xlsx", 0, true, 5, "", "", true, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
            xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(2);


            range = xlWorkSheet.UsedRange;
            rw = range.Rows.Count;
            cl = range.Columns.Count;

            var stokMiktari = (double)(range.Cells[2, 3] as Excel.Range).Value2;
            var fireMiktari = (double)(range.Cells[2, 4] as Excel.Range).Value2;
            var reyondaKalanMiktar = (double)(range.Cells[2, 5] as Excel.Range).Value2;

            //AddXY value in chart1 in series named as Salary  
            chart1.Series["Veriler"].Points.AddXY("Stok Miktarı", stokMiktari);
            chart1.Series["Veriler"].Points.AddXY("Fire Miktarı", fireMiktari);
            chart1.Series["Veriler"].Points.AddXY("Reyonda Kalan Miktar", reyondaKalanMiktar);
            //chart title  
            chart1.Titles.Add("Stok Bilgi Grafiği");
        }

        private void Grafik_Load(object sender, EventArgs e)
        {
            fillChart();
        }
    }
}
