﻿namespace Proje.Forms
{
    partial class FormStokBilgisi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblUrunAdi = new System.Windows.Forms.Label();
            this.lblStokMiktari = new System.Windows.Forms.Label();
            this.lblFireMiktari = new System.Windows.Forms.Label();
            this.lblReyondaKalan = new System.Windows.Forms.Label();
            this.cmbUrunler = new System.Windows.Forms.ComboBox();
            this.txtStok = new System.Windows.Forms.TextBox();
            this.txtFireMiktari = new System.Windows.Forms.TextBox();
            this.txtReyondaKalanMiktar = new System.Windows.Forms.TextBox();
            this.btnGoster = new System.Windows.Forms.Button();
            this.btnCikis = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblUrunAdi
            // 
            this.lblUrunAdi.AutoSize = true;
            this.lblUrunAdi.Location = new System.Drawing.Point(103, 31);
            this.lblUrunAdi.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblUrunAdi.Name = "lblUrunAdi";
            this.lblUrunAdi.Size = new System.Drawing.Size(63, 17);
            this.lblUrunAdi.TabIndex = 0;
            this.lblUrunAdi.Text = "Ürün Adı";
            // 
            // lblStokMiktari
            // 
            this.lblStokMiktari.AutoSize = true;
            this.lblStokMiktari.Location = new System.Drawing.Point(83, 78);
            this.lblStokMiktari.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblStokMiktari.Name = "lblStokMiktari";
            this.lblStokMiktari.Size = new System.Drawing.Size(81, 17);
            this.lblStokMiktari.TabIndex = 1;
            this.lblStokMiktari.Text = "Stok Miktarı";
            // 
            // lblFireMiktari
            // 
            this.lblFireMiktari.AutoSize = true;
            this.lblFireMiktari.Location = new System.Drawing.Point(89, 124);
            this.lblFireMiktari.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblFireMiktari.Name = "lblFireMiktari";
            this.lblFireMiktari.Size = new System.Drawing.Size(77, 17);
            this.lblFireMiktari.TabIndex = 2;
            this.lblFireMiktari.Text = "Fire Miktarı";
            // 
            // lblReyondaKalan
            // 
            this.lblReyondaKalan.AutoSize = true;
            this.lblReyondaKalan.Location = new System.Drawing.Point(17, 171);
            this.lblReyondaKalan.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblReyondaKalan.Name = "lblReyondaKalan";
            this.lblReyondaKalan.Size = new System.Drawing.Size(147, 17);
            this.lblReyondaKalan.TabIndex = 3;
            this.lblReyondaKalan.Text = "Reyonda Kalan Miktar";
            // 
            // cmbUrunler
            // 
            this.cmbUrunler.FormattingEnabled = true;
            this.cmbUrunler.Location = new System.Drawing.Point(201, 31);
            this.cmbUrunler.Margin = new System.Windows.Forms.Padding(4);
            this.cmbUrunler.Name = "cmbUrunler";
            this.cmbUrunler.Size = new System.Drawing.Size(192, 24);
            this.cmbUrunler.TabIndex = 4;
            this.cmbUrunler.SelectedIndexChanged += new System.EventHandler(this.cmbUrunler_SelectedIndexChanged);
            // 
            // txtStok
            // 
            this.txtStok.Location = new System.Drawing.Point(201, 76);
            this.txtStok.Margin = new System.Windows.Forms.Padding(4);
            this.txtStok.Name = "txtStok";
            this.txtStok.Size = new System.Drawing.Size(192, 22);
            this.txtStok.TabIndex = 5;
            // 
            // txtFireMiktari
            // 
            this.txtFireMiktari.Location = new System.Drawing.Point(201, 121);
            this.txtFireMiktari.Margin = new System.Windows.Forms.Padding(4);
            this.txtFireMiktari.Name = "txtFireMiktari";
            this.txtFireMiktari.Size = new System.Drawing.Size(192, 22);
            this.txtFireMiktari.TabIndex = 6;
            // 
            // txtReyondaKalanMiktar
            // 
            this.txtReyondaKalanMiktar.Location = new System.Drawing.Point(201, 165);
            this.txtReyondaKalanMiktar.Margin = new System.Windows.Forms.Padding(4);
            this.txtReyondaKalanMiktar.Name = "txtReyondaKalanMiktar";
            this.txtReyondaKalanMiktar.Size = new System.Drawing.Size(192, 22);
            this.txtReyondaKalanMiktar.TabIndex = 7;
            // 
            // btnGoster
            // 
            this.btnGoster.Location = new System.Drawing.Point(413, 28);
            this.btnGoster.Margin = new System.Windows.Forms.Padding(4);
            this.btnGoster.Name = "btnGoster";
            this.btnGoster.Size = new System.Drawing.Size(100, 50);
            this.btnGoster.TabIndex = 8;
            this.btnGoster.Text = "Grafikte Göster";
            this.btnGoster.UseVisualStyleBackColor = true;
            this.btnGoster.Click += new System.EventHandler(this.btnGoster_Click);
            // 
            // btnCikis
            // 
            this.btnCikis.Location = new System.Drawing.Point(249, 197);
            this.btnCikis.Margin = new System.Windows.Forms.Padding(4);
            this.btnCikis.Name = "btnCikis";
            this.btnCikis.Size = new System.Drawing.Size(109, 36);
            this.btnCikis.TabIndex = 9;
            this.btnCikis.Text = "Çıkış";
            this.btnCikis.UseVisualStyleBackColor = true;
            this.btnCikis.Click += new System.EventHandler(this.btnCikis_Click);
            // 
            // FormStokBilgisi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(529, 286);
            this.Controls.Add(this.btnCikis);
            this.Controls.Add(this.btnGoster);
            this.Controls.Add(this.txtReyondaKalanMiktar);
            this.Controls.Add(this.txtFireMiktari);
            this.Controls.Add(this.txtStok);
            this.Controls.Add(this.cmbUrunler);
            this.Controls.Add(this.lblReyondaKalan);
            this.Controls.Add(this.lblFireMiktari);
            this.Controls.Add(this.lblStokMiktari);
            this.Controls.Add(this.lblUrunAdi);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FormStokBilgisi";
            this.Text = "Stok Bilgisi";
            this.Load += new System.EventHandler(this.FormStokBilgisi_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblUrunAdi;
        private System.Windows.Forms.Label lblStokMiktari;
        private System.Windows.Forms.Label lblFireMiktari;
        private System.Windows.Forms.Label lblReyondaKalan;
        private System.Windows.Forms.ComboBox cmbUrunler;
        private System.Windows.Forms.TextBox txtStok;
        private System.Windows.Forms.TextBox txtFireMiktari;
        private System.Windows.Forms.TextBox txtReyondaKalanMiktar;
        private System.Windows.Forms.Button btnGoster;
        private System.Windows.Forms.Button btnCikis;
    }
}