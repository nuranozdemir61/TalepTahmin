﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proje.Forms
{
    public partial class FormYoneticiPaneli : Form
    {
        public FormYoneticiPaneli()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void btnStok_Click(object sender, EventArgs e)
        {
            FormStokBilgisi stokBilgisiForm = new FormStokBilgisi();
            stokBilgisiForm.Show();
        }

        private void btnTalepTahmini_Click(object sender, EventArgs e)
        {
            FormTalepTahmini talepTahminiForm = new FormTalepTahmini();
            talepTahminiForm.Show();
        }

        private void brtnSiparis_Click(object sender, EventArgs e)
        {
            FormSiparisVerme siparisForm = new FormSiparisVerme();
            siparisForm.Show();
        }

        private void btnUrunOnay_Click(object sender, EventArgs e)
        {
            FormUrunOnay urunOnayForm = new FormUrunOnay();
            urunOnayForm.Show();
        }
    }
}
