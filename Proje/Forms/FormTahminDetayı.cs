﻿using System;
using Excel = Microsoft.Office.Interop.Excel;
using System.Windows.Forms;
using System.IO;

namespace Proje.Forms
{
    public partial class FormTahminDetayı : Form
    {
        public FormTahminDetayı()
        {
            InitializeComponent();
        }

        private void btnWinterSonuc_Click(object sender, EventArgs e)
        {
            Excel.Application xlApp;
            Excel.Workbook xlWorkBook;
            Excel.Worksheet xlWorkSheet;
            Excel.Range range;

            xlApp = new Excel.Application();
            xlWorkBook = xlApp.Workbooks.Open(Directory.GetCurrentDirectory() +  @"\data\data.xlsx", 0, true, 5, "", "", true, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
            xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);


            range = xlWorkSheet.UsedRange;

            var wintersSonucu = (double)(range.Cells[19, 6] as Excel.Range).Value2;

            wintersSonuc.Text = wintersSonucu.ToString();
        }

        private void btnTrendAnalizi_Click(object sender, EventArgs e)
        {
            Excel.Application xlApp;
            Excel.Workbook xlWorkBook;
            Excel.Worksheet xlWorkSheet;
            Excel.Range range;

            xlApp = new Excel.Application();
            xlWorkBook = xlApp.Workbooks.Open(Directory.GetCurrentDirectory() + @"\data\data.xlsx", 0, true, 5, "", "", true, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
            xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);


            range = xlWorkSheet.UsedRange;

            var trendAnalizSonucu = (double)(range.Cells[15, 11] as Excel.Range).Value2;

            trendAnalizSonuc.Text = trendAnalizSonucu.ToString();
        }

        private void btnWGrafikGoster_Click(object sender, EventArgs e)
        {
            FormTahminGrafik formTahminGrafik = new FormTahminGrafik();
            formTahminGrafik.Show();
        }
    }
}
