﻿namespace Proje.Forms
{
    partial class FormUrunOnay
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmbUrunler = new System.Windows.Forms.ComboBox();
            this.lblUrunAdi = new System.Windows.Forms.Label();
            this.txtUrunMiktari = new System.Windows.Forms.TextBox();
            this.lblUrunMiktari = new System.Windows.Forms.Label();
            this.btnKaydet = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // cmbUrunler
            // 
            this.cmbUrunler.FormattingEnabled = true;
            this.cmbUrunler.Location = new System.Drawing.Point(160, 38);
            this.cmbUrunler.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cmbUrunler.Name = "cmbUrunler";
            this.cmbUrunler.Size = new System.Drawing.Size(160, 24);
            this.cmbUrunler.TabIndex = 5;
            this.cmbUrunler.SelectedIndexChanged += new System.EventHandler(this.cmbUrunler_SelectedIndexChanged);
            // 
            // lblUrunAdi
            // 
            this.lblUrunAdi.AutoSize = true;
            this.lblUrunAdi.Location = new System.Drawing.Point(85, 38);
            this.lblUrunAdi.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblUrunAdi.Name = "lblUrunAdi";
            this.lblUrunAdi.Size = new System.Drawing.Size(63, 17);
            this.lblUrunAdi.TabIndex = 4;
            this.lblUrunAdi.Text = "Ürün Adı";
            // 
            // txtUrunMiktari
            // 
            this.txtUrunMiktari.Location = new System.Drawing.Point(160, 101);
            this.txtUrunMiktari.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtUrunMiktari.Name = "txtUrunMiktari";
            this.txtUrunMiktari.Size = new System.Drawing.Size(160, 22);
            this.txtUrunMiktari.TabIndex = 9;
            // 
            // lblUrunMiktari
            // 
            this.lblUrunMiktari.AutoSize = true;
            this.lblUrunMiktari.Location = new System.Drawing.Point(23, 105);
            this.lblUrunMiktari.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblUrunMiktari.Name = "lblUrunMiktari";
            this.lblUrunMiktari.Size = new System.Drawing.Size(126, 17);
            this.lblUrunMiktari.TabIndex = 8;
            this.lblUrunMiktari.Text = "Gelen Ürün Miktarı";
            // 
            // btnKaydet
            // 
            this.btnKaydet.Location = new System.Drawing.Point(241, 166);
            this.btnKaydet.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnKaydet.Name = "btnKaydet";
            this.btnKaydet.Size = new System.Drawing.Size(101, 44);
            this.btnKaydet.TabIndex = 11;
            this.btnKaydet.Text = "Kaydet";
            this.btnKaydet.UseVisualStyleBackColor = true;
            this.btnKaydet.Click += new System.EventHandler(this.btnKaydet_Click);
            // 
            // FormUrunOnay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(379, 321);
            this.Controls.Add(this.btnKaydet);
            this.Controls.Add(this.txtUrunMiktari);
            this.Controls.Add(this.lblUrunMiktari);
            this.Controls.Add(this.cmbUrunler);
            this.Controls.Add(this.lblUrunAdi);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "FormUrunOnay";
            this.Text = "Ürün Onay";
            this.Load += new System.EventHandler(this.FormUrunOnay_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbUrunler;
        private System.Windows.Forms.Label lblUrunAdi;
        private System.Windows.Forms.TextBox txtUrunMiktari;
        private System.Windows.Forms.Label lblUrunMiktari;
        private System.Windows.Forms.Button btnKaydet;
    }
}