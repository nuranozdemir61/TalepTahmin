﻿namespace Proje.Forms
{
    partial class FormTahminDetayı
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnWinterSonuc = new System.Windows.Forms.Button();
            this.btnTrendAnalizi = new System.Windows.Forms.Button();
            this.btnWGrafikGoster = new System.Windows.Forms.Button();
            this.wintersSonuc = new System.Windows.Forms.TextBox();
            this.trendAnalizSonuc = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnWinterSonuc
            // 
            this.btnWinterSonuc.Location = new System.Drawing.Point(17, 48);
            this.btnWinterSonuc.Margin = new System.Windows.Forms.Padding(4);
            this.btnWinterSonuc.Name = "btnWinterSonuc";
            this.btnWinterSonuc.Size = new System.Drawing.Size(212, 39);
            this.btnWinterSonuc.TabIndex = 0;
            this.btnWinterSonuc.Text = "Winter\'s Yönetimi Sonucu";
            this.btnWinterSonuc.UseVisualStyleBackColor = true;
            this.btnWinterSonuc.Click += new System.EventHandler(this.btnWinterSonuc_Click);
            // 
            // btnTrendAnalizi
            // 
            this.btnTrendAnalizi.Location = new System.Drawing.Point(17, 116);
            this.btnTrendAnalizi.Margin = new System.Windows.Forms.Padding(4);
            this.btnTrendAnalizi.Name = "btnTrendAnalizi";
            this.btnTrendAnalizi.Size = new System.Drawing.Size(212, 36);
            this.btnTrendAnalizi.TabIndex = 1;
            this.btnTrendAnalizi.Text = "Trend Analizi Sonucu";
            this.btnTrendAnalizi.UseVisualStyleBackColor = true;
            this.btnTrendAnalizi.Click += new System.EventHandler(this.btnTrendAnalizi_Click);
            // 
            // btnWGrafikGoster
            // 
            this.btnWGrafikGoster.Location = new System.Drawing.Point(448, 48);
            this.btnWGrafikGoster.Margin = new System.Windows.Forms.Padding(4);
            this.btnWGrafikGoster.Name = "btnWGrafikGoster";
            this.btnWGrafikGoster.Size = new System.Drawing.Size(128, 41);
            this.btnWGrafikGoster.TabIndex = 2;
            this.btnWGrafikGoster.Text = "Grafiği Göster";
            this.btnWGrafikGoster.UseVisualStyleBackColor = true;
            this.btnWGrafikGoster.Click += new System.EventHandler(this.btnWGrafikGoster_Click);
            // 
            // wintersSonuc
            // 
            this.wintersSonuc.Location = new System.Drawing.Point(265, 57);
            this.wintersSonuc.Margin = new System.Windows.Forms.Padding(4);
            this.wintersSonuc.Name = "wintersSonuc";
            this.wintersSonuc.Size = new System.Drawing.Size(132, 22);
            this.wintersSonuc.TabIndex = 4;
            // 
            // trendAnalizSonuc
            // 
            this.trendAnalizSonuc.Location = new System.Drawing.Point(265, 122);
            this.trendAnalizSonuc.Margin = new System.Windows.Forms.Padding(4);
            this.trendAnalizSonuc.Name = "trendAnalizSonuc";
            this.trendAnalizSonuc.Size = new System.Drawing.Size(132, 22);
            this.trendAnalizSonuc.TabIndex = 5;
            // 
            // FormTahminDetayı
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(607, 295);
            this.Controls.Add(this.trendAnalizSonuc);
            this.Controls.Add(this.wintersSonuc);
            this.Controls.Add(this.btnWGrafikGoster);
            this.Controls.Add(this.btnTrendAnalizi);
            this.Controls.Add(this.btnWinterSonuc);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FormTahminDetayı";
            this.Text = "Tahmin Detayı";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnWinterSonuc;
        private System.Windows.Forms.Button btnTrendAnalizi;
        private System.Windows.Forms.Button btnWGrafikGoster;
        private System.Windows.Forms.TextBox wintersSonuc;
        private System.Windows.Forms.TextBox trendAnalizSonuc;
    }
}