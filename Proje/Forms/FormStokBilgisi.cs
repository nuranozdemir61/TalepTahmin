﻿using System;
using System.IO;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;

namespace Proje.Forms
{
    public partial class FormStokBilgisi : Form
    {
        public FormStokBilgisi()
        {
            InitializeComponent();
        }

        private void FormStokBilgisi_Load(object sender, EventArgs e)
        {
            cmbUrunler.Items.Add("Armut");
            cmbUrunler.Items.Add("Ayva");
            cmbUrunler.Items.Add("Biber");
            cmbUrunler.Items.Add("Çilek");
            cmbUrunler.Items.Add("Domates");
            cmbUrunler.Items.Add("Elma");
            cmbUrunler.Items.Add("Erik");
            cmbUrunler.Items.Add("Karpuz");
            cmbUrunler.Items.Add("Kavun");
            cmbUrunler.Items.Add("Patates");
            cmbUrunler.Items.Add("Salatalık");
            cmbUrunler.SelectedIndex = 0;
        }

        private void btnCikis_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cmbUrunler_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(cmbUrunler.SelectedIndex == 5)
            {

                Excel.Application xlApp;
                Excel.Workbook xlWorkBook;
                Excel.Worksheet xlWorkSheet;
                Excel.Range range;
                
                int rw = 0;
                int cl = 0;

                xlApp = new Excel.Application();
                xlWorkBook = xlApp.Workbooks.Open(Directory.GetCurrentDirectory() + @"\data\data.xlsx", 0, true, 5, "", "", true, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
                xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(2);


                range = xlWorkSheet.UsedRange;
                rw = range.Rows.Count;
                cl = range.Columns.Count;

                var stokMiktari = (double)(range.Cells[2, 3] as Excel.Range).Value2;
                var fireMiktari = (double)(range.Cells[2, 4] as Excel.Range).Value2;
                var reyondaKalanMiktar = (double)(range.Cells[2, 5] as Excel.Range).Value2;

                txtStok.Text = stokMiktari.ToString();
                txtFireMiktari.Text = fireMiktari.ToString();
                txtReyondaKalanMiktar.Text = reyondaKalanMiktar.ToString();

                
            }
        }

        private void btnGoster_Click(object sender, EventArgs e)
        {
            Grafik grafik = new Grafik();
            grafik.Show();

        }       
    }
}