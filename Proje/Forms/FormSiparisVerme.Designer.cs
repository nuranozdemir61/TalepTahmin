﻿namespace Proje.Forms
{
    partial class FormSiparisVerme
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmbUrunler = new System.Windows.Forms.ComboBox();
            this.lblUrunAdi = new System.Windows.Forms.Label();
            this.lblSiparisMiktari = new System.Windows.Forms.Label();
            this.txtSiparisMiktari = new System.Windows.Forms.TextBox();
            this.btnSiparisVer = new System.Windows.Forms.Button();
            this.btnCikis = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // cmbUrunler
            // 
            this.cmbUrunler.FormattingEnabled = true;
            this.cmbUrunler.Location = new System.Drawing.Point(191, 38);
            this.cmbUrunler.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cmbUrunler.Name = "cmbUrunler";
            this.cmbUrunler.Size = new System.Drawing.Size(189, 24);
            this.cmbUrunler.TabIndex = 5;
            this.cmbUrunler.SelectedIndexChanged += new System.EventHandler(this.cmbUrunler_SelectedIndexChanged);
            // 
            // lblUrunAdi
            // 
            this.lblUrunAdi.AutoSize = true;
            this.lblUrunAdi.Location = new System.Drawing.Point(117, 42);
            this.lblUrunAdi.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblUrunAdi.Name = "lblUrunAdi";
            this.lblUrunAdi.Size = new System.Drawing.Size(63, 17);
            this.lblUrunAdi.TabIndex = 4;
            this.lblUrunAdi.Text = "Ürün Adı";
            // 
            // lblSiparisMiktari
            // 
            this.lblSiparisMiktari.AutoSize = true;
            this.lblSiparisMiktari.Location = new System.Drawing.Point(25, 108);
            this.lblSiparisMiktari.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSiparisMiktari.Name = "lblSiparisMiktari";
            this.lblSiparisMiktari.Size = new System.Drawing.Size(155, 17);
            this.lblSiparisMiktari.TabIndex = 6;
            this.lblSiparisMiktari.Text = "Siparis Verilecek Miktar";
            // 
            // txtSiparisMiktari
            // 
            this.txtSiparisMiktari.Location = new System.Drawing.Point(189, 105);
            this.txtSiparisMiktari.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtSiparisMiktari.Name = "txtSiparisMiktari";
            this.txtSiparisMiktari.Size = new System.Drawing.Size(191, 22);
            this.txtSiparisMiktari.TabIndex = 7;
            // 
            // btnSiparisVer
            // 
            this.btnSiparisVer.Location = new System.Drawing.Point(165, 171);
            this.btnSiparisVer.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnSiparisVer.Name = "btnSiparisVer";
            this.btnSiparisVer.Size = new System.Drawing.Size(116, 44);
            this.btnSiparisVer.TabIndex = 8;
            this.btnSiparisVer.Text = "Sipariş Ver";
            this.btnSiparisVer.UseVisualStyleBackColor = true;
            this.btnSiparisVer.Click += new System.EventHandler(this.btnSiparisVer_Click);
            // 
            // btnCikis
            // 
            this.btnCikis.Location = new System.Drawing.Point(289, 171);
            this.btnCikis.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnCikis.Name = "btnCikis";
            this.btnCikis.Size = new System.Drawing.Size(115, 44);
            this.btnCikis.TabIndex = 9;
            this.btnCikis.Text = "Çıkış";
            this.btnCikis.UseVisualStyleBackColor = true;
            this.btnCikis.Click += new System.EventHandler(this.btnCikis_Click);
            // 
            // FormSiparisVerme
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(447, 321);
            this.Controls.Add(this.btnCikis);
            this.Controls.Add(this.btnSiparisVer);
            this.Controls.Add(this.txtSiparisMiktari);
            this.Controls.Add(this.lblSiparisMiktari);
            this.Controls.Add(this.cmbUrunler);
            this.Controls.Add(this.lblUrunAdi);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "FormSiparisVerme";
            this.Text = "Sipariş Verme";
            this.Load += new System.EventHandler(this.FormSiparisVerme_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbUrunler;
        private System.Windows.Forms.Label lblUrunAdi;
        private System.Windows.Forms.Label lblSiparisMiktari;
        private System.Windows.Forms.TextBox txtSiparisMiktari;
        private System.Windows.Forms.Button btnSiparisVer;
        private System.Windows.Forms.Button btnCikis;
    }
}