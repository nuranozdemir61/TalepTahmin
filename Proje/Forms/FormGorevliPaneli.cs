﻿using System;
using System.IO;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;

namespace Proje.Forms
{
    public partial class FormGorevliPaneli : Form
    {
        public FormGorevliPaneli()
        {
            InitializeComponent();

        }

        private void FormGorevliPaneli_Load(object sender, EventArgs e)
        {
            cmbUrunler.Items.Add("Armut");
            cmbUrunler.Items.Add("Ayva");
            cmbUrunler.Items.Add("Biber");
            cmbUrunler.Items.Add("Çilek");
            cmbUrunler.Items.Add("Domates");
            cmbUrunler.Items.Add("Elma");
            cmbUrunler.Items.Add("Erik");
            cmbUrunler.Items.Add("Karpuz");
            cmbUrunler.Items.Add("Kavun");
            cmbUrunler.Items.Add("Patates");
            cmbUrunler.Items.Add("Salatalık");
            cmbUrunler.SelectedIndex = 0;

            cmbTarih.Items.Add("06.05.2018");
            cmbTarih.Items.Add("13.05.2018");
            cmbTarih.Items.Add("20.05.2018");
            cmbTarih.Items.Add("27.05.2018");
            cmbTarih.Items.Add("29.04.2018");
            cmbTarih.Items.Add("22.04.2018");

            cmbTarih.SelectedIndex = 0;

        }

        private void btnCikis_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtFireMiktari_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(txtFireMiktari.Text))
            {
                cmbTarih.SelectedItem = "22.04.2018";
            }

        }

        private void btnKaydet_Click(object sender, EventArgs e)
        {
            try
            {
                string kaynak = Directory.GetCurrentDirectory() + @"\data\data.xlsx";
                Excel.Application xl = new Excel.Application();
                Excel.Workbook book = (Excel.Workbook)xl.ActiveWorkbook;
                book = (Excel.Workbook)(xl.Workbooks.Open(kaynak, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing,
                    Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing));

                Excel.Sheets sheet = book.Worksheets;
                Excel.Worksheet worksheet = (Excel.Worksheet)sheet.get_Item(2);


                int column = worksheet.UsedRange.Rows.Count;
                column++;

                worksheet.Cells[column, 1] = cmbUrunler.SelectedItem;
                worksheet.Cells[column, 2] = cmbTarih.SelectedItem;
                worksheet.Cells[column, 4] = txtFireMiktari.Text;

                book.Save();
                xl.Quit();
            }

            catch (Exception msg)
            {
                MessageBox.Show(msg.Message);
            }

        }

    }
}
