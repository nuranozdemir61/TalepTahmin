﻿using System;
using Excel = Microsoft.Office.Interop.Excel;
using System.Windows.Forms;
using System.IO;

namespace Proje.Forms
{
    public partial class FormUrunOnay : Form
    {
        public FormUrunOnay()
        {
            InitializeComponent();
        }

        private void FormUrunOnay_Load(object sender, EventArgs e)
        {
            cmbUrunler.Items.Add("Armut");
            cmbUrunler.Items.Add("Ayva");
            cmbUrunler.Items.Add("Biber");
            cmbUrunler.Items.Add("Çilek");
            cmbUrunler.Items.Add("Domates");
            cmbUrunler.Items.Add("Elma");
            cmbUrunler.Items.Add("Erik");
            cmbUrunler.Items.Add("Karpuz");
            cmbUrunler.Items.Add("Kavun");
            cmbUrunler.Items.Add("Patates");
            cmbUrunler.Items.Add("Salatalık");
            cmbUrunler.SelectedIndex = 0;
        }

        private void cmbUrunler_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbUrunler.SelectedIndex == 5)
            {
                Excel.Application xlApp;
                Excel.Workbook xlWorkBook;
                Excel.Worksheet xlWorkSheet;
                Excel.Range range;

                xlApp = new Excel.Application();
                xlWorkBook = xlApp.Workbooks.Open(Directory.GetCurrentDirectory() + @"\data\data.xlsx", 0, true, 5, "", "", true, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
                xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(2);

                range = xlWorkSheet.UsedRange;

                var siparisMiktari = (double)(range.Cells[2, 7] as Excel.Range).Value2;

                txtUrunMiktari.Text = siparisMiktari.ToString();
            }
        }

      
        private void btnKaydet_Click(object sender, EventArgs e)
        {
            string kaynak = Directory.GetCurrentDirectory() + @"\data\data.xlsx";
            Excel.Application xl = new Excel.Application();
            Excel.Workbook book = (Excel.Workbook)xl.ActiveWorkbook;
            book = (Excel.Workbook)(xl.Workbooks.Open(kaynak, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing,
                Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing));

            Excel.Sheets sheet = book.Worksheets;
            Excel.Worksheet worksheet = (Excel.Worksheet)sheet.get_Item(2);
            

            worksheet.Cells[2, 5] = txtUrunMiktari.Text;

            book.Save();
            xl.Quit();
        }
    }
}
