﻿using System;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;

namespace Proje.Forms
{
    public partial class FormTalepTahmini : Form
    {
        public FormTalepTahmini()
        {
            InitializeComponent();
        }

        private void FormTalepTahmini_Load(object sender, EventArgs e)
        {
            cmbUrunler.Items.Add("Armut");
            cmbUrunler.Items.Add("Ayva");
            cmbUrunler.Items.Add("Biber");
            cmbUrunler.Items.Add("Çilek");
            cmbUrunler.Items.Add("Domates");
            cmbUrunler.Items.Add("Elma");
            cmbUrunler.Items.Add("Erik");
            cmbUrunler.Items.Add("Karpuz");
            cmbUrunler.Items.Add("Kavun");
            cmbUrunler.Items.Add("Patates");
            cmbUrunler.Items.Add("Salatalık");
            cmbUrunler.SelectedIndex = 0;


            cmbBaslangicTarihi.Items.Add("01.04.2018");
            cmbBaslangicTarihi.Items.Add("08.04.2018");
            cmbBaslangicTarihi.Items.Add("15.04.2018");
            cmbBaslangicTarihi.Items.Add("22.04.2018");

            cmbBitisTarihi.Items.Add("01.04.2018");
            cmbBitisTarihi.Items.Add("08.04.2018");
            cmbBitisTarihi.Items.Add("15.04.2018");
            cmbBitisTarihi.Items.Add("22.04.2018");
            cmbBitisTarihi.Items.Add("29.04.2018");

        }

        private void btnTahminDetayı_Click(object sender, EventArgs e)
        {
            FormTahminDetayı talepTahminForm = new FormTahminDetayı();
            talepTahminForm.Show();
        }

        private void cmbBaslangicTarihi_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(cmbBaslangicTarihi.SelectedIndex == 3)
            {
                cmbBitisTarihi.SelectedIndex = 4;
            }
        }

        private void btnTalepTahmini_Click(object sender, EventArgs e)
        {
            Excel.Application xlApp;
            Excel.Workbook xlWorkBook;
            Excel.Worksheet xlWorkSheet;
            Excel.Range range;
            

            xlApp = new Excel.Application();
            xlWorkBook = xlApp.Workbooks.Open(Directory.GetCurrentDirectory() + @"\data\data.xlsx", 0, true, 5, "", "", true, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
            xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);


            range = xlWorkSheet.UsedRange;

            var talepTahmin = (string)(range.Cells[19, 12] as Excel.Range).Value2;

            talepTahmini.Text = talepTahmin;
        }

        private void talepTahmini_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
